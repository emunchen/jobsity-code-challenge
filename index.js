function funWithAnagrams(text) {
  let array = [];

  for (let word of text) {
    for (let i = 0; i < text.length; i++) {
      if (word != text[i] && !array.includes(word)) {
        if (getAnagramComparation(word, text[i])) {
          array.push(text[i]);
        }
      }
    }
  }

  return text.filter((n) => !array.includes(n)).sort();
}

const getAnagramComparation = (word, wordToComp) => {
  if (word.length != wordToComp.length) {
    return false;
  }

  const arrWord = word.split("").sort();
  const arrWordToComp = wordToComp.split("").sort();

  return arrWord.toString() === arrWordToComp.toString();
};

console.log(funWithAnagrams(["code", "aaagmnrs", "anagrams", "doce"]));
